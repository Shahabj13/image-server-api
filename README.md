# README
Image Server and API

### How do I get set up?
1. Run chmod +x image-server-setup.sh
2. Run sudo ./image-server-setup.sh

OR

1. Run command $sudo docker build -t image_server .
2. sudo docker run -d --name image_server -p 8000:8000 -v /images:/images image_server
3. Image server now accessible at address [host]:8000
4. Stop container with "sudo docker container stop image_server"
5. Start container again with "sudo docker container start image_server"
6. List all existing docker containers (that are running or stopped) with the command "sudo docker container ls -a"


**FYI: BitBucket doesn't show the line breaks in Markdown documents for some reason, so please click "Open Raw" OR view in a text editor**


### Converting images to base64
- Easiest way is to open the image in the following cyberchef recipe https://gchq.github.io/CyberChef/#recipe=To_Base64('A-Za-z0-9%2B/%3D') then copy the output


### API Usage

## GET /images/<filename>
Send GET HTTP request with filename in the URL to retrieve the base64 string in the following JSON string literal:
{
	"response": "success"/"error",
	"image": "image as a base64 string"
}

In the event that the image cannot be found, the JSON string will be the following:
{
	"response": "success"/"error",
	"message": "message value"
}

message value will be one of the following:
1. "file io error"
2. "file does not exist"
3. "insecure filename used"


## GET /images/thumbnail/<filename>
Send GET HTTP request with filename in the URL to retrieve the base64 string of the thumbnail for that image in the following JSON string literal:
{
	"response": "success/error",
	"image": "thumbnail of image as a base64 string"
}

In the event that the image thumbnail cannot be found, the JSON string will be the following:
{
	"response": "success"/"error",
	"message": "message value"
}

message value will be one of the following:
1. "file io error"
2. "file does not exist"
3. "insecure filename used"


## POST /upload
Send POST HTTP request with the following JSON string literal in the body of the request:
{
	"filename": "filename e.g., cat.jpeg",
	"image": "image as a base64 string"
}


The response to this request will be a HTTP request with the following JSON string literal in the body of the request:
{
	"response": "success"/"error",
	"message": "message value"
}

message value will be one of the following:
1. "image uploaded successfully"
2. "filename already exists"
3. "image failed to upload"
4. "file extension is not allowed"
5. "file extension mismatch between filename extension and image"
6. "insecure filename used"
7. "invalid base64 string"


## DELETE /delete/<filename>
Send DELETE request with the filename of the file to delete. The request will return the following JSON string literal in the body of the request:
{
	"response": "success/error",
	"message": "message value"
}

message value will be on of the following:
1. image deleted successfully
2. file does not exist on server

**Example test cases for using the API can be found in test_cases.md**


### Contribution guidelines
* Open a pull request and Jack will confirm


### Who do I talk to?
* Contact Jack for support