FROM python:3.9-slim

WORKDIR /image_server

COPY api.py ./
COPY requirements.txt ./

RUN apt-get update && apt-get install -y python3-opencv
RUN pip3 install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "--bind", "0.0.0.0:8000", "api:app"]