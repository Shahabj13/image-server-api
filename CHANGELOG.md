### 23/09/2021
- Added new /images/thumbnail/<filename> route to retrieve thumbnails
- Change max res of thumbnails from 100,100 to 200,200
- POST /delete/<filename> now uses the DELETE HTTP method i.e., DELETE /delete/<filename>
- README.md is now up to date with latest changes
- Made api code more module by separating image retrieval function